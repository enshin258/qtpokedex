import QtQuick 2.0
import Felgo 3.0
import "../logic"

Item {

    property string pokemonAllInfoHTTP: "https://pokeapi.co/api/v2/pokemon/"
    property string pokemonDescriptionHTTP: "https://pokeapi.co/api/v2/pokemon-species/"

    signal failedRequest()

    function getPokemonAllInfo(pokemonName, callback){
        sendRequest(pokemonName,pokemonAllInfoHTTP,callback)
    }
    function getPokemonDescription(pokemonName, callback){
        sendRequest(pokemonName,pokemonDescriptionHTTP,callback)
    }

    function sendRequest(pokemonName,HTTP,callback) {
        var tempPokemonName = pokemonName.toLowerCase();
        HttpRequest.get(HTTP + tempPokemonName)
        .then(function(result) {
            var response = result.text
            try {
                var JSONObj = JSON.parse(response)
            }
            catch (ex) {
                console.exception("Error while parsing JSON: " + ex)
                failedRequest()
                return
            }
            callback(JSONObj)
        })
        .catch(function(err) {
             console.error("Error messsage" + err)
             console.log("Fire failed rquest signal()")
             failedRequest()
        })
    }

}
