import QtQuick 2.0
import Felgo 3.0
AppText{
    font.family: "Helvetica"
    font.italic: true
    font.bold: true
    wrapMode: Text.WordWrap
    verticalAlignment: Text.AlignVCenter
    horizontalAlignment: Text.AlignHCenter
    anchors.margins: dp(15)
    fontSize: sp(14)
}
