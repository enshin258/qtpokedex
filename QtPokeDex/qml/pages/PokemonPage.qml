import QtQuick 2.0
import Felgo 3.0
import QtQuick.Controls 1.4

import "../dataModel"
import"../elements"

Page {
    id: page
    property var pokemonData: null
    title: qsTr(pokemonData.name)

    ImageGenerator{
        id: imageGenerator
    }
    Rectangle {
        id: backGround
        width: parent.width
        height: parent.height
        Image {
            id: imageBackground
            width: parent.width
            height: parent.height
            anchors.centerIn: parent
            source: imageGenerator.generateBackgroundBasedOnTypeOfPokemon(pokemonData.types[0].name)
        }
    }
    Rectangle{
        id: pokemonImageBorder
        anchors.left: parent.left
        anchors.top: parent.top
        anchors.margins: dp(10)
        width: parent.width/2
        height: parent.height/3
        border.width: dp(2)
        border.color: "black"
        radius: 90
        gradient: Gradient{
            GradientStop{position: 0.0; color: "moccasin"}
            GradientStop{position: 0.5; color: "oldlace"}
            GradientStop{position: 0.6; color: "oldlace"}
            GradientStop{position: 0.7; color: "oldlace"}
            GradientStop{position: 1; color: "moccasin"}
        }
        Image {
            id: pokemonImage
            width: parent.width
            height: parent.height
            antialiasing: true
            anchors.centerIn: parent
            source: pokemonData.image
            fillMode: Image.PreserveAspectFit
            smooth: true
        }
    }
    MyAppText{
        id: pokemonDescription
        width: parent.width/2
        height: parent.height/3
        anchors.top: parent.top
        anchors.left: pokemonImageBorder.right
        anchors.right: parent.right
        anchors.bottom: pokemonTypesBorder.top
        text: qsTr(pokemonData.description)
    }

    Rectangle{
        id: pokemonTypesBorder
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: pokemonImageBorder.bottom
        anchors.margins: dp(20)
        width: parent.width
        height: parent.height/5
        border.width: dp(2)
        border.color: "black"
        radius: 25
        gradient: Gradient{
            GradientStop{position: 0.0; color: "moccasin"}
            GradientStop{position: 0.5; color: "oldlace"}
            GradientStop{position: 0.6; color: "oldlace"}
            GradientStop{position: 0.7; color: "oldlace"}
            GradientStop{position: 1; color: "moccasin"}
        }
        Rectangle{
            anchors.verticalCenter: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width/4
            height: parent.height/5
            border.width: dp(2)
            border.color: "black"
            radius:25
            gradient: Gradient{
                GradientStop{position: 0.0; color: "moccasin"}
                GradientStop{position: 0.5; color: "oldlace"}
                GradientStop{position: 0.6; color: "oldlace"}
                GradientStop{position: 0.7; color: "oldlace"}
                GradientStop{position: 1; color: "moccasin"}
            }
            MyAppText{
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                height: parent.height
                text: qsTr("Types")
            }

        }
        Row{
            id: pokemonTypesRow
            width:parent.width
            height: parent.height
            anchors.top: pokemonImageBorder.bottom
            Image {
                id: pokemonTypeIcon1
                width: parent.width/2
                height: parent.height-dp(25)
                antialiasing: true
                fillMode: Image.PreserveAspectFit
                anchors.verticalCenter: parent.verticalCenter
                smooth: true
                source: imageGenerator.generateImageBasedOnTypeOfPokemon(pokemonData.types[0].name)
            }
            Image {
                id: pokemonTypeIcon2
                width: parent.width/2
                height: parent.height-dp(25)
                antialiasing: true
                fillMode: Image.PreserveAspectFit
                anchors.verticalCenter: parent.verticalCenter
                smooth: true
                source: ""
                Component.onCompleted: {
                    if(pokemonData.types.length > 1 ){
                        source=imageGenerator.generateImageBasedOnTypeOfPokemon(pokemonData.types[1].name)
                    }
                    else
                    {
                        pokemonTypeIcon2.visible = false
                        pokemonTypeIcon1.anchors.centerIn = parent
                    }
                }
            }
        }
    }
    Rectangle{

        id: pokemonStatsBorder
        anchors.top: pokemonTypesBorder.bottom
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.margins: dp(20)
        anchors.topMargin: dp(20)
        width: parent.width
        height: (parent.height * 1/6)
        border.width: dp(2)
        border.color: "black"
        radius: 25
        gradient: Gradient{
            GradientStop{position: 0.0; color: "moccasin"}
            GradientStop{position: 0.5; color: "oldlace"}
            GradientStop{position: 0.6; color: "oldlace"}
            GradientStop{position: 0.7; color: "oldlace"}
            GradientStop{position: 1; color: "moccasin"}
        }
        Rectangle{
            anchors.verticalCenter: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width/3
            height: parent.height/5
            border.width: dp(2)
            border.color: "black"
            radius:25
            gradient: Gradient{
                GradientStop{position: 0.0; color: "moccasin"}
                GradientStop{position: 0.5; color: "oldlace"}
                GradientStop{position: 0.6; color: "oldlace"}
                GradientStop{position: 0.7; color: "oldlace"}
                GradientStop{position: 1; color: "moccasin"}
            }
            MyAppText{
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                height: parent.height
                text: qsTr("Statistics")
            }

        }
        Column{
            width: parent.width/2
            height: parent.height
            anchors.left: parent.left
            MyAppText{
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                height: parent.height/3
                text: qsTr("Hp: " + pokemonData.stats[5].base_stat )
            }
            MyAppText{
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                height: parent.height/3
                text: qsTr("Attack: " + pokemonData.stats[4].base_stat)
            }
            MyAppText{
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                height: parent.height/3
                text: qsTr("Defense: " + pokemonData.stats[3].base_stat)
            }
        }
        Column{
            width: parent.width/2
            height: parent.height
            anchors.right: parent.right
            MyAppText{
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                height: parent.height/3
                text: qsTr("Speed: " + pokemonData.stats[0].base_stat )
            }
            MyAppText{
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                height: parent.height/3
                text: qsTr("Special Attack: " + pokemonData.stats[2].base_stat)
            }
            MyAppText{
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                height: parent.height/3
                text: qsTr("Special Defense: " + pokemonData.stats[1].base_stat)
            }
        }

    }
    Rectangle{
        id: pokemonAbilitiesBorder
        anchors.left: parent.left
        anchors.right: parent.right
        anchors.top: pokemonStatsBorder.bottom
        anchors.margins: dp(20)
        width: parent.width
        height: parent.height/6
        border.width: dp(2)
        border.color: "black"
        radius: 25
        gradient: Gradient{
            GradientStop{position: 0.0; color: "moccasin"}
            GradientStop{position: 0.5; color: "oldlace"}
            GradientStop{position: 0.6; color: "oldlace"}
            GradientStop{position: 0.7; color: "oldlace"}
            GradientStop{position: 1; color: "moccasin"}
        }
        Rectangle{
            anchors.verticalCenter: parent.top
            anchors.horizontalCenter: parent.horizontalCenter
            width: parent.width/3
            height: parent.height/5
            border.width: dp(2)
            border.color: "black"
            radius:25
            gradient: Gradient{
                GradientStop{position: 0.0; color: "moccasin"}
                GradientStop{position: 0.5; color: "oldlace"}
                GradientStop{position: 0.6; color: "oldlace"}
                GradientStop{position: 0.7; color: "oldlace"}
                GradientStop{position: 1; color: "moccasin"}
            }
            MyAppText{
                anchors.horizontalCenter: parent.horizontalCenter
                width: parent.width
                height: parent.height
                text: qsTr("ABILITIES")
            }

        }
        Column{
            id: abilitiesColumn
            width: parent.width
            height: parent.height
            anchors.verticalCenter: parent.verticalCenter
            anchors.top: parent.top
            anchors.topMargin: dp(20)
            spacing: dp(10)
            Component.onCompleted: {
                var component
                component = Qt.createComponent("../elements/MyAppText.qml")
                for(var obj in pokemonData.abilities){
                    var abilityName = pokemonData.abilities[obj].name
                    var text
                    text = component.createObject(abilitiesColumn,{text: qsTr(abilityName)})
                }
                for (var item in children){
                    children[item].anchors.horizontalCenter =  abilitiesColumn.horizontalCenter;
                    }
            }
        }
    }
}


/*##^##
Designer {
    D{i:31;invisible:true}
}
##^##*/
