import QtQuick 2.0
import Felgo 3.0

Page{

    useSafeArea: false


    NavigationStack {
        id: navigationStack
        Connections {
            target: dataModel
            onDataCreated:{
                console.debug("Going to create new page based on pokemonData")
                navigationStack.popAllExceptFirstAndPush(pokemonPage,{pokemonData: pokemonData})


            }
            onCannotCreatePokemonData:{
                searchBar.text=""
                shakeAnimation.start()
                dataModel.noError = true
            }
        }

        Page {
            id: page
            title: qsTr("QtPokeDex")
            Rectangle {
                width: parent.width
                height: parent.height
                gradient: Gradient{
                    GradientStop{position: 0.0; color: "moccasin"}
                    GradientStop{position: 0.5; color: "oldlace"}
                    GradientStop{position: 0.6; color: "oldlace"}
                    GradientStop{position: 0.7; color: "oldlace"}
                    GradientStop{position: 1; color: "moccasin"}
                }
            }

            Image {
                id: logoImage
                source: "../../assets/pokemon.png"
                fillMode: Image.PreserveAspectFit
                width: parent.width
                height: parent.height/2
            }

            Component {
              id: pokemonPage
              PokemonPage {}
            }

            SearchBar {
                id: searchBar
                placeHolderText: shakeAnimation.running? qsTr("Cant find Pokemon! Try again :)") :  qsTr("Type name or number of Pokemon")
                placeHolderColor: shakeAnimation.running ? "red" : "grey"
                iconColor: shakeAnimation.running? "red" : "grey"
                barBackgroundColor: "oldlace"
                anchors.top: parent.verticalCenter
                onAccepted: {
                    logic.searchPokemonByName(searchBar.text)
                }
                SequentialAnimation {
                    id:shakeAnimation
                    running: false
                    NumberAnimation { target: searchBar; property: "x"; to: dp(6); duration: 60 }
                    NumberAnimation { target: searchBar; property: "x"; to: -dp(6); duration: 60 }
                    NumberAnimation { target: searchBar; property: "x"; to: dp(4); duration: 40 }
                    NumberAnimation { target: searchBar; property: "x"; to: -dp(4); duration: 40 }
                    NumberAnimation { target: searchBar; property: "x"; to: dp(2); duration: 40 }
                    NumberAnimation { target: searchBar; property: "x"; to: -dp(2); duration: 40 }
                    NumberAnimation { target: searchBar; property: "x"; to: dp(0); duration: 400 }
                    NumberAnimation { target: searchBar; property: "x"; to: -dp(0); duration: 400 }



                }
            }
            AppText {
                text: qsTr("Enter name of Pokemon, like: charizard, pikachu etc. You can also enter number of pokemon")
                width: parent.width
                anchors.top: searchBar.bottom
                color: "gray"
                padding: dp(10)
                wrapMode: Text.WordWrap
            }
        }
    }

}


