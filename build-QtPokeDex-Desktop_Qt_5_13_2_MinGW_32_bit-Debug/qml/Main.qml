import Felgo 3.0
import QtQuick 2.0
import "pages"
import "logic"
import "dataModel"
    //link to pokeball image autor <div>Icons made by <a href="https://www.flaticon.com/authors/nikita-golubev" title="Nikita Golubev">Nikita Golubev</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>

// <div>Icons made by <a href="https://www.flaticon.com/authors/those-icons" title="Those Icons">Those Icons</a> from <a href="https://www.flaticon.com/" title="Flaticon">www.flaticon.com</a></div>
App {

    id: app
    readonly property var basicPadding: dp(Theme.navigationBar.defaultBarItemPadding)
    onInitTheme: {
       Theme.colors.tintColor = "#cc0000"
    }

    Logic {
        id: logic
    }

    DataModel {
        id: dataModel
        dispatcher: logic
    }

    SearchPage{
    }
}
