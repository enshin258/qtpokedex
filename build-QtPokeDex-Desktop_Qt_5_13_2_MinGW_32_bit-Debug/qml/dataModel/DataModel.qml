import QtQuick 2.0
import Felgo 3.0

Item {

    property alias dispatcher: logicConnections.target

    signal dataCreated(var pokemonData)
    signal cannotCreatePokemonData()

    property string pokemonName: ""
    property string pokemonColor: ""
    property var pokemonAbilities: null
    property string pokemonDescription: ""
    property string pokemonImage: ""
    property var pokemonTypes: null
    property var pokemonStats: null

    property bool noError: true

    HTTPRequester{
        id: httpRequester
    }

    Connections{
        id: logicConnections
        onSearchPokemonByName: {
            httpRequester.getPokemonDescription(pokemonName,_.prepareDescriptionOfPokemon)
            httpRequester.getPokemonAllInfo(pokemonName,_.prepareDataAboutPokemon)
        }
    }
    Connections{
        target: httpRequester
        onFailedRequest:{
            if(noError){
                noError = false
                cannotCreatePokemonData()
            }
        }
    }

    Item{
        id: _

        function prepareDescriptionOfPokemon(dataObject){

            pokemonColor = dataObject.color.name

            for(var i=0; i<dataObject.flavor_text_entries.length; i++){
                if(dataObject.flavor_text_entries[i].language.name === "en")
                {
                    pokemonDescription = dataObject.flavor_text_entries[i].flavor_text
                    break
                }
            }
            console.log("Succesfully created JSON with description of pokemont")
        }
        function prepareDataAboutPokemon(dataObject){

            pokemonName = dataObject.name
            pokemonImage = dataObject.sprites.front_default

            var types = []
            for(var obj in dataObject.types){
                types.push(dataObject.types[obj].type)
            }
            pokemonTypes = types

            var abilities = []
            for(var obj2 in dataObject.abilities){
                abilities.push(dataObject.abilities[obj2].ability)
            }
            pokemonAbilities = abilities

            var stats = []
            for(var obj3 in dataObject.stats){
                stats.push(dataObject.stats[obj3])
            }
            pokemonStats = stats
            console.log("Succesfully created JSON with data about pokemon")

            createFinalData()
        }
        function createFinalData(){
            var pokemonData = ({
                                 "name": pokemonName,
                                 "color": pokemonColor,
                                 "abilities": pokemonAbilities,
                                 "description": pokemonDescription,
                                 "image": pokemonImage,
                                 "types": pokemonTypes,
                                 "stats" : pokemonStats
                                 })
            console.log(JSON.stringify(pokemonData,null," "))
            dataCreated(pokemonData)
            noError = true
        }
    }
}
